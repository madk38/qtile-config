#!/bin/bash

# Polkit authentication agent
/usr/lib/polkit-kde-authentication-agent-1 &

dunst &

if [[ "$XDG_SESSION_TYPE" == "wayland" ]]; then
	gammastep &
else
	# Enable natural scrolling
	xinput set-prop "$(xinput | grep 'DELL.*Touchpad' | awk '{print $6}' | sed 's/id=//g')" 'libinput Natural Scrolling Enabled' 1

	# Set backgroup wallpaper
	feh --bg-fill --no-fehbg --randomize "$HOME"/Pictures/Wallpapers/*

	# Autostart applications
	picom -b

	redshift &
fi
