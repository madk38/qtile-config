from libqtile import layout

from .colors import colors

layout_config = {
    "border_focus": colors.green,
    "border_normal": colors.bg0,
    "border_width": 2,
}

layouts = [
    layout.Columns(**layout_config),
    layout.Max(**layout_config),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    layout.MonadTall(ratio=0.64, **layout_config),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(columnwidth=700, property_big='1.0'),
]
