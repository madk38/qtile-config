from libqtile.config import DropDown, Group, Match, ScratchPad

from .defaults import terminal

groups = [
    ScratchPad(
        "scratchpad",
        [
            DropDown(
                "term",
                terminal + " -o window.opacity=0.8",
                x=0,
                y=0.6,
                width=1,
                height=0.4,
                on_focus_lost_hide=True,
            ),
        ],
    ),
    Group("u", label="", layout="monadtall", position=1),
    Group(
        "i",
        layout="max",
        spawn=["firefox"],
        matches=[Match(wm_class=["firefox", "brave"])],
        label="󰈹",
        init=False,
        position=2,
    ),
    Group("o", label="", matches=[Match(title=["Okular"])], position=3),
    Group("p", label="󰕼", position=4),
    Group("a"),
    Group("s"),
    Group("d"),
    Group("f"),
]
