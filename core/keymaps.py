from libqtile.config import Key
from libqtile.lazy import lazy

from .defaults import (alt_browser, browser, file_manager, mod, scripts_dir,
                       terminal)

keys = [
    # =============
    # Switch between windows
    # =============
    Key([mod], "h", lazy.layout.left()),
    Key([mod], "l", lazy.layout.right()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "k", lazy.layout.up()),
    # =============
    # Shift windows
    # =============
    Key([mod, "shift"], "h", lazy.layout.shuffle_left()),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right()),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up()),
    Key([mod], "m", lazy.layout.shuffle_left()),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "n", lazy.layout.toggle_split()),  # Column Layout only
    # =============
    # Grow or Shrink windows.
    # =============
    Key([mod, "control"], "h", lazy.layout.grow_left()),
    Key([mod, "control"], "l", lazy.layout.grow_right()),
    Key([mod, "control"], "j", lazy.layout.grow_down()),
    Key([mod, "control"], "k", lazy.layout.grow_up()),
    Key([mod, "control"], "n", lazy.layout.normalize()),
    # =============
    # Toggle between different layouts as defined below
    # =============
    Key([mod], "Tab", lazy.next_layout()),
    Key([mod, "shift"], "Tab", lazy.prev_layout()),
    Key([mod], "space", lazy.window.toggle_fullscreen()),
    # =============
    # =============
    Key([mod], "w", lazy.window.kill()),
    Key([mod], "r", lazy.restart()),
    Key([mod, "control"], "q", lazy.shutdown()),
    # =============
    # Launch applications
    # =============
    Key([mod], "Return", lazy.spawn(terminal)),
    Key([mod], "n", lazy.spawn(browser)),
    Key([mod], "b", lazy.spawn(alt_browser)),
    Key([mod], "e", lazy.spawn(file_manager)),
    Key([], "F1", lazy.group["scratchpad"].dropdown_toggle("term")),
    Key(
        [mod],
        "semicolon",
        lazy.spawn(f"{scripts_dir}/launcher"),
    ),
    # =============
    # System shortcuts
    # =============
    Key([], "XF86AudioMute", lazy.spawn("pamixer -t")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("pamixer -i 5")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("pamixer -d 5")),
    Key(
        [],
        "XF86Calculator",
        lazy.spawn("pamixer --toggle-mute --default-source"),
    ),
    Key([], "XF86MonBrightnessUp", lazy.spawn("brightnessctl -e s +5%")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("brightnessctl -e s 5%-")),
    # =============
    # Screenshot
    # =============
    Key([], "Print", lazy.spawn("flameshot gui")),
    Key(["shift"], "Print", lazy.spawn("flameshot full")),
    Key(["control", "shift"], "Print", lazy.spawn("flameshot full --clipboard")),
]

# =============
# Windows and Groups(Workspaces)
# =============
groups = "uiopasdf"
for i in groups:
    keys.extend(
        [
            Key([mod], i, lazy.group[i].toscreen(toggle=True)),
            Key([mod, "shift"], i, lazy.window.togroup(i, switch_group=True)),
        ]
    )
