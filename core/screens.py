from libqtile.config import Screen

from .bars import topbar

screens = [
    Screen(
        top=topbar,
        wallpaper="/home/kanhaiya/Pictures/Wallpapers/elion-jashari-I34HAJM2RCo-unsplash.jpg",
        wallpaper_mode="fill",
    ),
]
