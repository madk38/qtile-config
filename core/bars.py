import subprocess

from libqtile.bar import Bar
from libqtile.widget import (Backlight, Battery, Bluetooth, CheckUpdates,
                             Chord, Clock, CurrentLayoutIcon, GenPollText,
                             GroupBox, Prompt, PulseVolume, Sep,
                             StatusNotifier, TaskList, TextBox, Volume, Wlan)

from .colors import colors


def my_func(text):
    for string in [" - Brave", " — Mozilla Firefox"]:
        text = text.replace(string, "")
    return text


def get_mic_status():
    res = subprocess.run(
        ["pamixer", "--get-mute", "--default-source"], capture_output=True
    ).stdout
    if b"true" in res:
        return "󰍭"
    return ""


widget_defaults = dict(
    font="MesloLGS Nerd Font Mono",
    fontsize=18,
    padding=4,
    foreground=colors.white,
)

sep = Sep(foreground=colors.grey, size_percent=60, padding=6)

topbar = Bar(
    [
        CurrentLayoutIcon(scale=0.50, padding=0),
        GroupBox(
            active=colors.blue0,
            highlight_method="line",
            center_aligned=True,
            this_current_screen_border=colors.blue0,
            hide_unused=True,
            spacing=0,
            padding=2,
            fontsize=22,
        ),
        sep,
        Prompt(),
        StatusNotifier(),
        TaskList(
            highlight_method="block",
            icon_size=18,
            margin=0,
            parse_text=my_func,
            markup_focused="[ {} ]",
            border=colors.bg_highlight,
        ),
        Chord(
            chords_colors={
                "launch": ("#ff0000", "#ffffff"),
            },
            name_transform=lambda name: name.upper(),
        ),
        CheckUpdates(
            display_format="󰏕",
            colour_have_updates=colors.teal,
            colour_no_updates=colors.red,
            fmt="{}",
        ),
        CheckUpdates(display_format="{updates}", fmt="{}"),
        Battery(
            foreground=colors.green,
            low_foreground=colors.red,
            charge_char="󰂄",
            discharge_char="󰁾",
            empty_char="󰂃",
            full_char="󰁹",
            unknown_char="󰂎",
            format="{char}",
            fontsize=15,
        ),
        Battery(
            low_foreground=colors.red,
            format="{percent:2.0%}",
            low_percentage=0.15,
            notify_below=15,
        ),
        TextBox(foreground=colors.blue0, text="󰕾", fontsize=22),
        Volume(fmt="{}"),
        GenPollText(
            func=get_mic_status,
            foreground=colors.blue0,
            update_interval=0.5,
            fontsize=22,
        ),
        TextBox(foreground=colors.yellow, text="󰃞", fontsize=25),
        Backlight(
            fmt="{}",
            backlight_name="intel_backlight",
        ),
        sep,
        Bluetooth(
            foreground=colors.blue0,
            hci="/dev_00_16_94_3D_5F_69",
            fmt="󰂯",
            fontsize=15,
        ),
        Bluetooth(
            hci="/dev_00_16_94_3D_5F_69",
            fmt="{}",
        ),
        Wlan(
            interface="wlan0",
            disconnected_message="󰖪",
            foreground=colors.magenta,
            format="󰖩",
            fontsize=24,
        ),
        Wlan(
            format="{essid}",
            interface="wlan0",
        ),
        sep,
        TextBox(
            foreground=colors.cyan,
            text="󰅐",
            fontsize=22,
        ),
        Clock(format="%a, %d %b %I:%M %p "),
    ],
    28,
    background=colors.black,
)
